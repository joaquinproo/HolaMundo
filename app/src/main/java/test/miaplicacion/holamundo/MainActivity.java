package test.miaplicacion.holamundo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ActionMenuItemView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText editTextUsername, editTextPassword;
    private Button buttonLogin;
    private TextView textViewPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.editTextUsername = (EditText) findViewById(R.id.etUsername);
        this.editTextPassword = (EditText) findViewById(R.id.etPassword);

        this.buttonLogin = (Button) findViewById(R.id.btLogin);

        this.textViewPassword = (TextView) findViewById(R.id.tvPassword);

        this.buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtener contraseña

                String password = editTextPassword.getText().toString();

                // Mostrar Contraseña
                textViewPassword.setText(password);

                //Mostrar Toast

                Toast.makeText(getApplicationContext(),"Password" + password, Toast.LENGTH_SHORT).show();

                // logs

                Log.v("Hola Mundo","La persona puso el password:" + password);


            }
        });


    }
}
